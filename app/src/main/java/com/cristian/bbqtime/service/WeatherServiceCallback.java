package com.cristian.bbqtime.service;

import com.cristian.bbqtime.data.Channel;
import com.cristian.bbqtime.data.Forecast;

/**
 * Created by Cristian on 27.07.2017.
 */
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel, Forecast forecast);
    void serviceFailure(Exception exception);
}
