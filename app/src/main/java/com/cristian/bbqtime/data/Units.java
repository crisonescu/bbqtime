package com.cristian.bbqtime.data;

import org.json.JSONObject;

/**
 * Created by Cristian on 27.07.2017.
 */
public class Units implements JSONPopulator {

    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature");
    }
}
