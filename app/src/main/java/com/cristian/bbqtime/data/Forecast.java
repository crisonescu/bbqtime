package com.cristian.bbqtime.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cristian on 28.07.2017.
 */
public class Forecast {

    List<ForecastDaily> forecastDailyList;

    public List<ForecastDaily> getForecastDailyList() {
        return forecastDailyList;
    }

    public void populateArray(JSONArray dataArray) throws JSONException {

        forecastDailyList = new ArrayList<ForecastDaily>();


        if(5 <= dataArray.length()) {
            //starting from one because we already have the first day displayed first
            for (int i = 0; i < 5; i++) { // Walk through the Array.
                JSONObject forecastDay = dataArray.getJSONObject(i);
                forecastDailyList.add(new ForecastDaily(forecastDay.optInt("code"),
                        forecastDay.optString("date"),
                        forecastDay.optString("day"),
                        forecastDay.optInt("high"),
                        forecastDay.optInt("low"),
                        forecastDay.optString("text")));
            }
        }
    }
}
