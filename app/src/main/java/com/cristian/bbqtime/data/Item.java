package com.cristian.bbqtime.data;

import org.json.JSONObject;

/**
 * Created by Cristian on 27.07.2017.
 */
public class Item implements JSONPopulator {
    private ConditionToday conditionToday;

    public ConditionToday getConditionToday() {
        return conditionToday;
    }

    @Override
    public void populate(JSONObject data) {
        conditionToday = new ConditionToday();
        conditionToday.populate(data.optJSONObject("condition"));
    }
}
