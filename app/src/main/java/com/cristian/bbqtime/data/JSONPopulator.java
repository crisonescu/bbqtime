package com.cristian.bbqtime.data;

import org.json.JSONObject;

/**
 * Created by Cristian on 27.07.2017.
 */
public interface JSONPopulator {
    void populate(JSONObject data);
}
