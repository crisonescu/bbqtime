package com.cristian.bbqtime.data;

import org.json.JSONObject;

/**
 * Created by Cristian on 27.07.2017.
 */
public class ConditionToday implements JSONPopulator {
    private int code;
    private int temperature;
    private String date;
    private String description;

    public int getCode() {
        return code;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public void populate(JSONObject data) {
        code = data.optInt("code");
        temperature = data.optInt("temp");
        date = data.optString("date");
        description = data.optString("text");
    }
}
