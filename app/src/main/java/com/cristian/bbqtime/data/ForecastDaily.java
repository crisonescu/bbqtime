package com.cristian.bbqtime.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Cristian on 28.07.2017.
 */
public class ForecastDaily {
    private int code;
    private Date date;
    private String day;
    private int high;
    private int low;
    private String description;


    public int getCode() {
        return code;
    }

    public Date getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }

    public int getHigh() {
        return high;
    }

    public int getLow() {
        return low;
    }

    public String getDescription() {
        return description;
    }

    public ForecastDaily(int code, String date, String day, int high, int low, String description) {
        this.code = code;
        this.date = convertToSdf(date);
        this.day = day;
        this.high = high;
        this.low = low;
        this.description = description;
    }

    private Date convertToSdf(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        Date dateFormat = null;

        try {
            dateFormat = sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat;
    }
}
