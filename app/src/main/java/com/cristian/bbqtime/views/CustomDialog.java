package com.cristian.bbqtime.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cristian.bbqtime.R;
import com.cristian.bbqtime.data.ForecastDaily;
import com.cristian.bbqtime.views.wearCompat.NotificationBuilder;

import java.util.List;

/**
 * Created by Cristian on 29.07.2017.
 */
public class CustomDialog {

    Activity activity;
    LinearLayout fiveDaysForecast;

    public void displayData(List<ForecastDaily> forecastList){
        fiveDaysForecast.removeAllViews();

        for(final ForecastDaily forecastDay : forecastList) {
            LinearLayout day = new LinearLayout(activity);
            day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra("title", "Chill and BBQ");
                    intent.putExtra("description", "BBQ set on this day");
                    intent.putExtra("beginTime", forecastDay.getDate().getTime());
                    intent.putExtra("allDay", true);
                    activity.startActivity(intent);

                    new NotificationBuilder(activity).notificationShow("Chill and BBQ", forecastDay.getDate());
                }
            });

            TextView codeTextView = new TextView(activity);
            codeTextView.setGravity(Gravity.CENTER);
            codeTextView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "font/weather.ttf"));
            codeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
            codeTextView.setText(activity.getResources().getText(activity.getResources().getIdentifier("code_" + String.valueOf(forecastDay.getCode()), "string", activity.getPackageName())));

            TextView dayTextView = new TextView(activity);
            dayTextView.setGravity(Gravity.CENTER);
            dayTextView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "font/Comfortaa-Bold.ttf"));
            dayTextView.setText(forecastDay.getDay());

            TextView highTextView = new TextView(activity);
            highTextView.setGravity(Gravity.CENTER);
            highTextView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "font/Comfortaa-Regular.ttf"));
            highTextView.setText(String.valueOf(forecastDay.getHigh()) + " " + '\u00B0');

            TextView lowTextView = new TextView(activity);
            lowTextView.setGravity(Gravity.CENTER);
            lowTextView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "font/Comfortaa-Regular.ttf"));
            lowTextView.setText(String.valueOf(forecastDay.getLow()) + " " + '\u00B0');

            TextView descriptionTextView = new TextView(activity);
            descriptionTextView.setGravity(Gravity.CENTER);
            descriptionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
            descriptionTextView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "font/Comfortaa-Light.ttf"));
            descriptionTextView.setText(forecastDay.getDescription());

            day.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER_HORIZONTAL));
            day.setOrientation(LinearLayout.VERTICAL);
            day.addView(codeTextView);
            day.addView(dayTextView);
            day.addView(highTextView);
            day.addView(lowTextView);
            day.addView(descriptionTextView);
            day.setWeightSum(0.1f);


            fiveDaysForecast.addView(day);
            fiveDaysForecast.setGravity(Gravity.CENTER_VERTICAL);
        }
    }


    public void showDialog(Activity activity, String msg, List<ForecastDaily>forecastList){

        this.activity = activity;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_layout);
        fiveDaysForecast = (LinearLayout) dialog.findViewById(R.id.result_layout);
        displayData(forecastList);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
