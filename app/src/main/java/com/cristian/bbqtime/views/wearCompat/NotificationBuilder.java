package com.cristian.bbqtime.views.wearCompat;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.cristian.bbqtime.R;

import java.util.Date;

/**
 * Created by Cristian on 31.07.2017.
 */
public class NotificationBuilder {

    Activity mContext;

    public NotificationBuilder(Activity mContext){
        this.mContext = mContext;
    }

    public void notificationShow(String title, Date date){
        //standard notification code
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText("BBQ set to happen on " + date.toString());

            Intent notifyNextTimeIntent = new Intent(mContext, WearActionReceiver.class);
            notifyNextTimeIntent.putExtra(WearActionReceiver.NOTIFICATION_ID_STRING, 123);
            notifyNextTimeIntent.putExtra(WearActionReceiver.WEAR_ACTION, WearActionReceiver.SNOOZE_NOTIFICATION);

            //Create new WearableExtender object and add actions
            NotificationCompat.WearableExtender extender = new NotificationCompat.WearableExtender();

            //Extend Notification builder
            mBuilder.extend(extender);

        //Get notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        //show notification
        mNotificationManager.notify(123, mBuilder.build());
    }
}
