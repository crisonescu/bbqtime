package com.cristian.bbqtime.views;

import android.app.Application;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cristian.bbqtime.data.Channel;
import com.cristian.bbqtime.data.Forecast;
import com.cristian.bbqtime.data.ForecastDaily;
import com.cristian.bbqtime.data.Item;
import com.cristian.bbqtime.service.YahooWeatherService;

import java.util.List;

/**
 * Created by Cristian on 02.08.2017.
 */
public class WeatherLayoutUpdater {

    private Application context;
    public List<ForecastDaily> forecastList;

    public WeatherLayoutUpdater(Application context){
        this.context = context;
    }

    public void updateLayout(Channel channel, Forecast forecast, YahooWeatherService service, LinearLayout fiveDaysForecast, TextView weatherIcon, TextView temperature, TextView date, TextView conditionToday, TextView location){
        Item item = channel.getItem();

        weatherIcon.setText(context.getResources().getText(context.getResources().getIdentifier("code_" + item.getConditionToday().getCode(), "string", context.getPackageName())));
        temperature.setText(item.getConditionToday().getTemperature() + " " + '\u00B0' + channel.getUnits().getTemperature());
        temperature.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Bold.ttf"));
        date.setText(item.getConditionToday().getDate());
        conditionToday.setText(item.getConditionToday().getDescription());
        conditionToday.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Light.ttf"));
        location.setText(service.getLocation());
        location.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));

        forecastList = forecast.getForecastDailyList();

        for(ForecastDaily forecastDay : forecast.getForecastDailyList()){
            //ignore first day because we already have the data.
            if(!forecastDay.getDay().equals(forecast.getForecastDailyList().get(0).getDay())) {
                LinearLayout day = new LinearLayout(context.getApplicationContext());
                LinearLayout temperatures = new LinearLayout(context.getApplicationContext());

                TextView codeTextView = new TextView(context.getApplicationContext());
                codeTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/weather.ttf"));
                codeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40);
                codeTextView.setGravity(Gravity.CENTER);
                codeTextView.setText(context.getResources().getText(context.getResources().getIdentifier("code_" + String.valueOf(forecastDay.getCode()), "string", context.getPackageName())));

                TextView dayTextView = new TextView(context.getApplicationContext());
                dayTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Bold.ttf"));
                dayTextView.setGravity(Gravity.CENTER);
                dayTextView.setText(forecastDay.getDay());

                TextView highTextView = new TextView(context.getApplicationContext());
                highTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));
                highTextView.setText(String.valueOf(forecastDay.getHigh()) + " " + '\u00B0');

                TextView lowTextView = new TextView(context.getApplicationContext());
                lowTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Regular.ttf"));
                lowTextView.setText(String.valueOf(forecastDay.getLow()) + " " + '\u00B0');

                TextView descriptionTextView = new TextView(context.getApplicationContext());
                descriptionTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), "font/Comfortaa-Light.ttf"));
                descriptionTextView.setGravity(Gravity.CENTER);
                descriptionTextView.setText(forecastDay.getDescription());
                descriptionTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

                temperatures.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
                temperatures.setOrientation(LinearLayout.VERTICAL);
                temperatures.setGravity(Gravity.CENTER);
                temperatures.addView(highTextView);
                temperatures.addView(lowTextView);

                day.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
                day.setOrientation(LinearLayout.VERTICAL);
                day.setPadding(10,0,10,0);
                day.setGravity(Gravity.CENTER);
                day.addView(dayTextView);
                day.addView(codeTextView);
                day.addView(temperatures);
                day.addView(descriptionTextView);

                fiveDaysForecast.addView(day);
            }
        }
    }

}
