package com.cristian.bbqtime;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cristian.bbqtime.data.Channel;
import com.cristian.bbqtime.data.Forecast;
import com.cristian.bbqtime.data.ForecastDaily;
import com.cristian.bbqtime.logic.BarbecueLogicClass;
import com.cristian.bbqtime.service.WeatherServiceCallback;
import com.cristian.bbqtime.service.YahooWeatherService;
import com.cristian.bbqtime.views.CustomDialog;
import com.cristian.bbqtime.views.WeatherLayoutUpdater;

import java.util.List;

public class WeatherActivity extends AppCompatActivity implements WeatherServiceCallback {

    private LinearLayout fiveDaysForecast;
    private LinearLayout todayWeather;

    private TextView weatherIcon;
    private TextView temperature;
    private TextView conditionToday;
    private TextView location;
    private TextView date;

    private YahooWeatherService service;
    private ProgressDialog dialog;

    public List<ForecastDaily> forecastList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        fiveDaysForecast = (LinearLayout) findViewById(R.id.five_days_forecast);
        todayWeather     = (LinearLayout) findViewById(R.id.today_weather);

        weatherIcon     = (TextView) findViewById(R.id.weatherIcon);
        temperature     = (TextView) findViewById(R.id.temperature);
        conditionToday  = (TextView) findViewById(R.id.conditionToday);
        location        = (TextView) findViewById(R.id.location);
        date            = (TextView) findViewById(R.id.date);

        Typeface type = Typeface.createFromAsset(getAssets(),"font/weather.ttf");
        weatherIcon.setTypeface(type);
        weatherIcon.setText(getApplicationContext().getString(R.string.code_3200));

        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getText(getResources().getIdentifier("loading_weather_data", "string", getPackageName())));
        dialog.show();

        service = new YahooWeatherService(this);
        service.refreshWeather("Timisoara, RO");
    }

    public void shouldIBBQ(View v){
        clearAll();

        dialog.setMessage(getResources().getText(getResources().getIdentifier("loading_weather_data", "string", getPackageName())));
        dialog.show();

        service = new YahooWeatherService(this);
        service.refreshWeather("Timisoara, RO");

        dialog.setMessage(getResources().getText(getResources().getIdentifier("hold_on_computing", "string", getPackageName())));
        new CustomDialog().showDialog(this, new BarbecueLogicClass(this, forecastList).mathForGreatestDayForBBQ(), forecastList);
        dialog.hide();

    }

    public void clearAll(){
        fiveDaysForecast.removeAllViews();
        weatherIcon.setText(getResources().getText(getResources().getIdentifier("code_3200", "string", getPackageName())));
        temperature.setText("N/A" + " " + '\u00B0' + "N/A");
        date.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));
        conditionToday.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));
        location.setText(getResources().getText(getResources().getIdentifier("NA", "string", getPackageName())));

    }

    @Override
    public void serviceSuccess(Channel channel, Forecast forecast) {
        dialog.hide();
        WeatherLayoutUpdater wlu = new WeatherLayoutUpdater(getApplication());
        wlu.updateLayout(channel, forecast, service, fiveDaysForecast, weatherIcon, temperature, date, conditionToday, location);
        forecastList = wlu.forecastList;
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG);
    }
}
